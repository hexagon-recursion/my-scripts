FROM fedora:latest
RUN dnf -y install direnv \
	gcc	libasan* libubsan* \
	xz \
	util-linux # runuser
RUN useradd --create-home user
RUN mkdir -p /src/cpython && chown -R user:user /src
RUN runuser user -c 'tar --directory /src/cpython -xJ --file "$(direnv fetchurl "https://www.python.org/ftp/python/3.10.2/Python-3.10.2.tar.xz" "sha256-F946x9qfJRmqnWQ3jGA6c6DprVjf+ogS5FFgwIbeZMc=")"'
ENV MY_SANITIZE=-fsanitize=address
RUN dnf install -y 'dnf-command(builddep)'
RUN dnf builddep -y python3
RUN export CXXFLAGS="$MY_SANITIZE" && \
	export CFLAGS="$MY_SANITIZE" && \
	export LDFLAGS="$MY_SANITIZE" && \
	export ASAN_OPTIONS=detect_leaks=0 && \
	cd /src/cpython/Python-3.10.2 && \
	runuser -u user -- ./configure && \
	runuser -u user -- make && \
	make install

RUN dnf -y install xorg-x11-server-Xspice xterm 

RUN mkdir -p /src/sdl && chown -R user:user /src
RUN runuser user -c 'tar --directory /src/sdl -x --gzip --file "$(direnv fetchurl "https://www.libsdl.org/release/SDL2-2.0.20.tar.gz" "sha256-xWq6HXtbDn6Znkp2mMcLY6M5T/lwS19uHFfgwW8E3QY=")"'
RUN dnf builddep -y SDL2 SDL2_image SDL2_mixer SDL2_ttf && dnf remove --noautoremove -y SDL2-devel
RUN SDL_ttf='https://github.com/libsdl-org/SDL_ttf/releases/download/release-2.0.18/SDL2_ttf-2.0.18.tar.gz' && \
	SDL_ttf="$(runuser -u user -- direnv fetchurl "$SDL_ttf" 'sha256-cjTriINRTgGed0fHA+SndFdbGNQ1wipKKdBoy3aKIlE=')" && \
	runuser -u user -- tar -x --gzip --directory /src --file "$SDL_ttf"
RUN SDL_image='https://github.com/libsdl-org/SDL_image/archive/refs/tags/release-2.0.5.tar.gz' && \
	SDL_image="$(runuser -u user -- direnv fetchurl "$SDL_image" 'sha256-drf2f0waX4NoZY8OHlm9qkVV0cx/OkQTF4zXNQGZg/8=')" && \
	runuser -u user -- tar -x --gzip --directory /src --file "$SDL_image"

ENV DISPLAY=:1.0
CMD Xspice --disable-ticketing "$DISPLAY" &> /tmp/log.txt & runuser -u user -- bash
